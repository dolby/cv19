import cv19/[cases, options, output]

proc main() =
    var
        data = loadCases()
        opts = parseCmdLineOpts()

    if opts.limit == -1:
        opts.limit = len(data) - 1
    elif opts.limit >= len(data):
        opts.limit = len(data) - 1

    if len(opts.filter) > 0:
        var filteredData = newSeq[Country]()
        for c in data:
            if opts.filter.find(c.countryName) != -1:
                filteredData.add(c)
        data = filteredData
        opts.limit = len(data) - 1

    print(opts, data)

main()