import json, httpClient

const
    url = "https://coronacache.home-assistant.io/corona.json"

type
    Country* = object
        id*: int
        countryName*: string 
        lastUpdate*: int
        confirmed*: int
        deaths*: int
        recovered*: int
        active*: int
        rate*: float

proc loadCases*(): seq[Country] =
    var
        client = newHttpClient()
        resp = client.getContent(url)

    let data = parseJson(resp)

    for record in data["features"]:
        var c: Country
        c.id = record["attributes"]["OBJECTID"].getInt()
        c.countryName = record["attributes"]["Country_Region"].getStr()
        c.lastUpdate = record["attributes"]["Last_Update"].getInt()
        c.confirmed = record["attributes"]["Confirmed"].getInt()
        c.deaths = record["attributes"]["Deaths"].getInt()
        c.recovered = record["attributes"]["Recovered"].getInt()
        c.active = record["attributes"]["Active"].getInt()
        c.rate = (c.deaths / c.confirmed) * 100
        result.add(c)
