import algorithm, strformat
import options, cases

proc dataSort(s: Sort): proc =
    case s:
    of country:
        return proc (a, b: Country): int =
            cmp(a.countryName, b.countryName)
    of confirmed:
        return proc (a, b: Country): int =
            if a.confirmed > b.confirmed: -1 else: 1
    of deaths:
        return proc (a, b: Country): int =
            if a.deaths > b.deaths: -1 else: 1
    of recovered:
        return proc (a, b: Country): int =
            if a.recovered > b.recovered: -1 else: 1

proc header() =
    echo &"""{"ID":<4}{"Country":<32}{"Confirmed":>10}{"Deaths":>10}{"Rate":>10}"""

proc rule() =
    echo "---+--------------------------------+---------+---------+----------"

proc print*(options: Options, data: var seq[Country]) =
    data.sort(dataSort(options.sortBy))
    header()
    rule()
    for i in 0 .. options.limit:
        echo fmt"{data[i].id:<4}{data[i].countryName:32}{data[i].confirmed:>10}{data[i].deaths:>10}{data[i].rate:>10.2f}"
    rule()
