import parseopt, strutils

type
    Sort* = enum
        country, confirmed, deaths, recovered
    Options* = object
        limit*: int
        filter*: seq[string]
        sortBy*: Sort

const
    help = """
Usage: cv19 [opts]

Options:
    -h, --help                          Print this help message
    -l, --limit=<Number>                Limit output to a number of rows
    -f, --filter=<CountryName, ...>     Comma separated list of countries to display
    -s, --sort=<Sort>                   Sort by: country (default), confirmed, deaths, recovered
"""
proc showHelp(exitCode=0) =
    echo help
    quit(exitCode)

proc initOptions(): Options =
    result.limit = -1
    result.sortBy = country
    result.filter = newSeq[string]()

proc parseCmdLineOpts*(): Options =
    result = initOptions()
    var opts = initOptParser(shortNoVal = {'h'}, longNoVal = @["help"])
    for kind, key, val in opts.getopt():
        case kind
        of cmdEnd: doAssert(false)
        of cmdShortOption, cmdLongOption:
            if val == "":
                if (key == "h") or (key == "help"):
                    showHelp()
                else:
                    showHelp(1)
            else:
                case key
                of "l", "limit":
                    result.limit = val.parseInt()
                of "f", "filter":
                    result.filter = val.split(',')
                of "s", "sort":
                    result.sortBy = parseEnum[Sort](val)
                else:
                    showHelp(1)
        of cmdArgument:
            showHelp(1)

