# Package

version       = "0.1.0"
author        = "Andrew Dolby"
description   = "Gets the lastest Coronavirus (COVID-19) data"
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["cv19"]



# Dependencies

requires "nim >= 1.0.6"
